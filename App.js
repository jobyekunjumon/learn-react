const heading = React.createElement(
  "h1",
  { id: "heading", key: 1 },
  "Hello World"
);
const heading2 = React.createElement(
  "h2",
  { id: "heading", key: 2 },
  "Hello World h2"
);
const container = React.createElement("div", { id: "container" }, [
  heading,
  heading2,
]);

console.log(container);

const root = ReactDOM.createRoot(document.getElementById("root"));

const root2 = ReactDOM.createRoot(document.getElementById("root2"));

root.render(container);
root2.render(heading);
